FROM node:18-alpine3.16

WORKDIR /usr/src/app

COPY package.json .
COPY package-lock.json . 

RUN npm install --no-optional && npm cache clean --force

COPY . .

EXPOSE 3080

CMD ["npm", "start" ]