import {
    addProduct,
    findProduct,
    updateProduct,
    deleteProduct,
} from './controller.js';

const Product = {
    type: 'object',
    properties: {
        name: {
            type: 'string',
        },
        productId: {
            type: 'string',
        },
    },
};

const successResponseMessage = {
    201: {
        type: 'object',
        properties: {
            message: { type: 'string' },
        },
    },
};

export default async function (fastify, opts) {
    const addProductOpts = {
        onRequest: [fastify.authenticate],
        schema: {
            body: Product,
        },
        handler: addProduct,
    };

    const findProductOpts = {
        onRequest: [fastify.authenticate],
        // schema: {
        //     response: successResponseMessage,
        // },
        handler: findProduct,
    };

    const updateProductOpts = {
        onRequest: [fastify.authenticate],
        handler: updateProduct,
    };

    const deleteProductOpts = {
        onRequest: [fastify.authenticate],
        handler: deleteProduct,
    };

    fastify.post('/product', addProductOpts);
    fastify.get('/product/:productId', findProductOpts);
    fastify.post('/product/:productId', updateProductOpts);
    fastify.delete('/product/:productId', deleteProductOpts);
}
