export async function addProduct(req, reply) {
    const { name, productId } = req.body;
    const shopeeProduct = this.shopeeStore.Product({ name, productId });
    let product = await this.shopeeStore.Product.findOne({ productId });
    if (!product) {
        this.log.info(`Saving Product: ${shopeeProduct}`);
        product = await shopeeProduct.save();
        reply.code(201).send(product);
    } else {
        const msg = `Product Already Exists ${product}`;
        reply.code(400).send(msg);
        this.log.info(msg);
    }
}

export async function findProduct(req, reply) {
    const { productId } = req.params;
    this.log.info(`Product ID ${productId}`);
    const shopeeProduct = await this.shopeeStore.Product.findOne({
        productId,
    });

    if (shopeeProduct) {
        this.log.info(`Shopee product name: ${shopeeProduct.name}`);
        reply.code(200).send(shopeeProduct);
    } else {
        const msg = `Product not found ${productId}`;
        reply.code(400).send(msg);
    }
}

export async function updateProduct(req, reply) {
    const { name } = req.body;
    const { productId } = req.params;

    const doc = await this.shopeeStore.Product.findOneAndUpdate(
        { productId },
        { name },
        { 'new': true }
    );

    if (doc) {
        reply.code(200).send(doc);
    } else {
        const msg = `Unable to find ${productId}`;
        reply.code(400).send(`${msg} ${doc}`);
    }
}

export async function deleteProduct(req, reply) {
    const { productId } = req.params;

    const doc = await this.shopeeStore.Product.deleteOne({
        productId,
    });

    if (doc.deletedCount === 1) {
        const msg = `Product ${productId} successfully deleted`;
        this.log.info(msg);
        reply.code(200).send(msg);
    } else {
        const msg = `Unable to delete product check if existing`;
        this.log.info(msg);
        reply.code(400).send(msg);
    }
}
