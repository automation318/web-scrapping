export default async function (fastify) {
  fastify.get('/', async (request, reply) => {
    fastify.log.fatal(`This is the root endoint`);
    return { root: true };
  });
}
