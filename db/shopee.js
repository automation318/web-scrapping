import * as mongoose from 'mongoose';

const ShopeePrdSchema = new mongoose.Schema({
  name: String,
  url: String,
  image: String,
  productId: String,
  searchTag: [],
  page: Number,
  price: String,
  lowPrice: String,
  highPrice: String,
  availability: String,
  bestRating: Number,
  worstRating: Number,
  ratingCount: Number,
  ratingValue: Number,
});

export async function getMongoDB(dbConnString, dbName) {
  mongoose.set('strictQuery', true);
  let dbModels = {};
  try {
    const db = await mongoose.connect(dbConnString, {
      dbName,
    });
    if (!db) throw new Error('cannot connect to database');

    dbModels = {
      db,
      Product: db.model('shopee-products', ShopeePrdSchema),
    };
  } catch (error) {
    throw new Error(error.message);
  }
  return dbModels;
}
