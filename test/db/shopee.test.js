import Fastify from 'fastify';
import t from 'tap';
import * as fastifyEnv from '@fastify/env';
import { v4 as uuidv4 } from 'uuid';
import { options } from '../../plugins/app-config.js';
import { getMongoDB } from '../../db/shopee.js';

async function getEnvs() {
  const fastify = Fastify();
  fastify.register(fastifyEnv, options);
  await fastify.ready();
  const env = fastify.config;
  fastify.close();
  return env;
}

t.before(async () => {
  const envs = await getEnvs();
  const shopeeDB = await getMongoDB(envs.SCRAPPER_DB, envs.SCRAPPER_DB_NAME);
  const shopeeProduct = new shopeeDB.Product({
    name: 'test product',
    url: 'https://shopee-product-test.com',
    productId: uuidv4(),
    searchTag: ['unit test product'],
    image: 'https://shopee-product-imageurl.com',
    page: 0,
    price: '0.00',
    lowPrice: '0.00',
    highPrice: '0.00',
    availability: 'InStock',
    bestRating: 5,
    worstRating: 1,
    ratingCount: '10',
    ratingValue: '5.00',
  });
  t.context.shopeeDB = shopeeDB;
  t.context.shopeeProduct = shopeeProduct;
});

t.teardown(async () => {
  t.context.shopeeDB.db.connection.close();
});

t.test('Test Shopee Product Crud', async () => {
  const {shopeeProduct, shopeeDB} = t.context;
  t.test('Save Product', async (t) => {
    try {
      const product = await shopeeProduct.save();
      // TODO: implement pino as logger
      console.info(`Product Saved: ${product}`);
      t.equal(shopeeProduct.productId, product.productId);
    } catch (e) {
      console.log(e.message);
    }
  });

  t.test('Get Product', async (t) => {
    try {
      const product = await shopeeDB.Product.findOne({
        productId: shopeeProduct.productId,
      });
      console.log(`Fetched Product: ${product}`);
      t.equal(shopeeProduct.productId, product.productId);
    } catch (e) {
      console.log(e.message);
    }
  });

  t.test('Delete Product', async (t) => {
    try {
      await shopeeProduct.deleteOne({ productId: shopeeProduct.productId });
      // TODO: add assertion here
      t.equal('equal', 'equal');
    } catch (e) {
      console.log(e.message);
    }
  });
});
