dev: 
	npm run dev

start:
	npm run start

tests:
	npm run test

swagger:
	npm run swagger

lint: 
	npm run lint

## up: starts all containers in the background without forcing build
up:
	@echo "Starting Docker images..."
	docker-compose up -d
	@echo "Docker images started!"

## down: stop docker compose
down: 
	@echo "Stopping docker compose..."
	docker-compose down
	@echo "Done!"