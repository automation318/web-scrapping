import fp from 'fastify-plugin';
import * as fastifyEnv from '@fastify/env';
import jwt from '@fastify/jwt';
import cors from '@fastify/cors';
import swagger from '@fastify/swagger';

// TODO: move schema to separate file
const schema = {
  type: 'object',
  required: ['FASTIFY_PORT', 'SCRAPPER_DB', 'SCRAPPER_DB_NAME'],
  properties: {
    SCRAPPER_DB: {
      type: 'string',
    },
    SCRAPPER_DB_NAME: {
      type: 'string',
    },
    FASTIFY_JWT_SECRET: {
      type: 'string',
    },
  },
};

export const options = {
  dotenv: true,
  schema,
};

export default fp(async (fastify, opts) => {
  // use await to use the config on jwt registration
  await fastify.register(fastifyEnv, options);

  await fastify.register(jwt, {
    secret: fastify.config.FASTIFY_JWT_SECRET,
  });

  await fastify.decorate('authenticate', async (request, reply) => {
    try {
      await request.jwtVerify();
    } catch (err) {
      reply.send(err);
    }
  });

  // TODO: implement csrf
  // cors options can also be on the respnse headers
  fastify.register(cors, {
    origin: '*',
    methods: ['POST', 'PUT', 'DELETE', 'GET'],
  });

  // swagger
  fastify.register(swagger, {
    swagger: {},
  });
});
