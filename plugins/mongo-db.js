import fp from 'fastify-plugin';
import { getMongoDB } from '../db/shopee.js';

export default fp(async (fastify) => {
  let shopeeDB = {};
  try {
    shopeeDB = await getMongoDB(
      fastify.config.SCRAPPER_DB,
      fastify.config.SCRAPPER_DB_NAME
    );
  } catch (err) {
    fastify.log.error(`error connecting to mongodb ${err.message}`);
    process.exit(1);
  }

  fastify
    .decorate('shopeeStore', shopeeDB)
    .addHook('onClose', (fastify, done) => {
      fastify.shopeeStore.db.connection.close();
    });
    
});
