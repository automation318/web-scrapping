# Getting Started with [Fastify-CLI](https://www.npmjs.com/package/fastify-cli)

This project was bootstrapped with Fastify-CLI.

## Available Scripts

In the project directory, you can run:

### `npm run dev`

To start the app in dev mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

### `npm start`

For production mode

### `npm run test`

Run the test cases.

## Learn More

To learn Fastify, check out the [Fastify documentation](https://www.fastify.io/docs/latest/).

## ESLint Airbnb style
https://dev.to/bigyank/a-quick-guide-to-setup-eslint-with-airbnb-and-prettier-3di2

    npm install eslint --save-dev
    npx eslint --init (key in options)

    npm install --save-dev --save-exact prettier

    add to .vscode settings.json
    {
    "editor.defaultFormatter": "esbenp.prettier-vscode",
    "[javascript]": {
        "editor.defaultFormatter": "esbenp.prettier-vscode"
    }
    }

    npm i eslint-config-prettier


## Pending Items 
    - logging (done)
    - swagger implementation  (done)
    - security (cors, jwt implementations for endpoints) in progress
    - update the readme 
    - prettier (done) 
    - es lint (done)
    - add codes for test code coverage
    - dockerize - (done)
    - research how to make mongo start on desired port
    - upload to gitlab (done)
    - gitlab cicd -
    - deploy the application into cloud -
    - add documentation in gitlab
    


## Docker
https://dev.to/francescoxx/crud-api-with-fastify-postgres-docker-1df3


